FROM golang:1.20.2-alpine3.17 as builder
RUN apt-get update && api-get install -y nodejs
RUN npm install -g sass
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN sass scss/main.scss css/main.css
RUN go generate ./...
RUN go build -o goci

FROM golang:1.20.2-alpine3.17
WORKDIR /app
COPY --from=builder /app/goci /app/goci

CMD ["./goci"]
