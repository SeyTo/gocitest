package main

import (
	"log"
	"net/http"
	"os"
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(os.Getenv("TEST_ENV")))
}

func main() {
	http.HandleFunc("/", getRoot)

	log.Println("Running at :3333")
	err := http.ListenAndServe(":3333", nil)
	if err != nil {
		panic(err)
	}
}
